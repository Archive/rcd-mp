/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <glib.h>
#include <gnet.h>

#include "magic-proxy-common.h"

#include <stdio.h>
#include <unistd.h>

typedef struct _MPSocketTunnel MPSocketTunnel;
struct _MPSocketTunnel {
    GTcpSocket      *socket;
    guint            watch;
    MPSocketTunnel  *remote;
    GSList         **connections;
};

/*
 * GNet calls g_io_channel_ref() on sockets before returning them.  Ian
 * previously just patched gnet, but I am not sure that the patch will
 * be accepted by the maintainer, and I'd rather just take the minor
 * performance hit and just unref it instead of relying on a patch.
 * (And then we can share a gnet with whomever else)
 */
GIOChannel *
mp_tcp_socket_get_iochannel (GTcpSocket *socket)
{
    GIOChannel *chan;

    g_return_val_if_fail (socket, NULL);

    chan = gnet_tcp_socket_get_iochannel (socket);

    g_io_channel_unref (chan);

    return chan;
}

/* Copies data from one socket to another, used in the client and
 * server components of the proxy */
gboolean
mp_socket_tunnel_func (GIOChannel *channel, GIOCondition condition,
                       gpointer user_data)
{
    MPSocketTunnel *mpst;
    gboolean error = FALSE;

    mpst = user_data;

    if (condition & G_IO_IN) {
        gsize bytes_read;
        gchar buf[BUFSIZE];
        GIOStatus status;

        /* We've potentially got some data to read on the socket
         * (although this may just be an indication that the other
         * side shutdown cleanly) */
        status = g_io_channel_read_chars (channel, buf, BUFSIZE, &bytes_read,
                                          NULL);
        MP_DEBUG ("Read %d bytes from fd %d\n", bytes_read,
                  g_io_channel_unix_get_fd (channel));

        switch (status) {
        case G_IO_STATUS_AGAIN:
            /* Do nothing, get called again */
            return TRUE;
        case G_IO_STATUS_EOF:
        case G_IO_STATUS_ERROR:
            /* Shut down this connection */
            error = TRUE;
            break;
        case G_IO_STATUS_NORMAL:
            /* Write the data to our remote */
            status = mp_io_channel_write_chars (
                mp_tcp_socket_get_iochannel (mpst->remote->socket), buf,
                bytes_read);

            switch (status) {
            case G_IO_STATUS_AGAIN:
                /* This shouldn't happen, since
                 * mp_io_channel_write_chars retries on _AGAIN */
                g_assert_not_reached ();
            case G_IO_STATUS_EOF:
            case G_IO_STATUS_ERROR:
                /* Something went wrong writing our data to the other
                 * side, so we'll shut this connection down and call
                 * it a wash */
                error = TRUE;
                break;
            case G_IO_STATUS_NORMAL:
                /* Our job here is done */
                break;
            }
            break;
        }
    }

    if (error || (condition & (G_IO_ERR | G_IO_HUP))) {
        MP_DEBUG ("Shutting down connection between %d and %d\n",
                  g_io_channel_unix_get_fd (channel),
                  g_io_channel_unix_get_fd (
                      mp_tcp_socket_get_iochannel (
                          mpst->remote->socket)));

        /* Shutdown the socket we were listening to */
        g_source_remove (mpst->watch);
        gnet_tcp_socket_unref (mpst->socket);
        *mpst->connections = g_slist_remove (*mpst->connections, mpst);

        /* Shutdown the socket we were writing to (our partner in
         * crime) */
        g_source_remove (mpst->remote->watch);
        gnet_tcp_socket_unref (mpst->remote->socket);
        *mpst->connections = g_slist_remove (*mpst->connections, mpst->remote);

        /* Free our now-orphaned data structures */
        g_free (mpst->remote);
        g_free (mpst);

        /* Doesn't really matter what we return, since we
         * g_source_remove'd our own id, but ... */
        return FALSE;
    }

    /* We do, of course, want to get called again */
    return TRUE;
}

void
mp_socket_tunnel_create (GTcpSocket *socket1, GTcpSocket *socket2,
                         GSList **connections)
{
    MPSocketTunnel *mpst1, *mpst2;

    mpst1 = g_new0 (MPSocketTunnel, 1);
    mpst2 = g_new0 (MPSocketTunnel, 1);

    mpst1->socket = socket1;
    gnet_tcp_socket_ref (socket1);

    mpst1->watch =
        g_io_add_watch (
            mp_tcp_socket_get_iochannel (socket1),
            G_IO_IN | G_IO_ERR | G_IO_HUP,
            mp_socket_tunnel_func, mpst1);

    mpst1->connections = connections;

    *connections = g_slist_append (*connections, mpst1);

    mpst2->socket = socket2;
    gnet_tcp_socket_ref (socket2);

    mpst2->watch =
        g_io_add_watch (
            mp_tcp_socket_get_iochannel (socket2),
            G_IO_IN | G_IO_ERR | G_IO_HUP,
            mp_socket_tunnel_func, mpst2);

    mpst2->connections = connections;

    *connections = g_slist_append (*connections, mpst2);

    mpst1->remote = mpst2;
    mpst2->remote = mpst1;
}

/* Create a listening socket and accept connections on it */
GTcpSocket *
mp_create_server (guint16 port, const gchar *name, guint retries, guint delay,
                  GTcpSocketAcceptFunc func, gpointer user_data)
{
    GInetAddr *addr, *iface;
    GTcpSocket *server = NULL;
    guint count;

    if (!name)
        iface = gnet_inetaddr_new_any ();
    else {
        addr = gnet_inetaddr_new (name, 0);
        iface = gnet_inetaddr_get_interface_to (addr);
        gnet_inetaddr_unref (addr);
    }

    gnet_inetaddr_set_port (iface, port);

    count = 0;
    while (!server && count < retries) {
        if (count)
            sleep (delay);
        server = gnet_tcp_socket_server_new_interface (iface);
        count++;
    }

    gnet_inetaddr_unref (iface);

    if (server)
        gnet_tcp_socket_server_accept_async (server, func, user_data);

    return server;
}

/* Like g_io_channel_write_chars, except it writes what we told it to
 * write, unless there was an actual error */
GIOStatus
mp_io_channel_write_chars (GIOChannel *channel, const gchar *buf, gsize count)
{
    gsize bytes_written, bytes_remaining;
    GIOStatus status;

    bytes_remaining = count;
    do {
        status = g_io_channel_write_chars (
            channel, buf + (count - bytes_remaining), bytes_remaining,
            &bytes_written, NULL);
        MP_DEBUG ("Wrote %d bytes to fd %d\n", bytes_written,
                  g_io_channel_unix_get_fd (channel));
        bytes_remaining -= bytes_written;
    } while (bytes_remaining > 0 && (status == G_IO_STATUS_NORMAL ||
                                     status == G_IO_STATUS_AGAIN));

    return status;
}
