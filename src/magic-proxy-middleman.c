/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <glib.h>
#include <glib-object.h>
#include <gnet.h>

#include "magic-proxy-common.h"

#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <rcd-module.h>
#include <rcd-prefs.h>
#include <rcd-shutdown.h>

#define CONTROL_PORT 2828
#define CLIENT_PORT  2829

typedef struct {
    GTcpSocket *control_server;
    GTcpSocket *control;
    guint       control_watch;
    GTcpSocket *client_server;
    guint16     sequence;
} MiddlemanInfo;

static RCDModule *rcd_module;
static MiddlemanInfo middleman_info;

/* Called when anything interesting happens to our control
 * connection */
static gboolean
control_func (GIOChannel *channel, GIOCondition condition, gpointer user_data)
{
    MiddlemanInfo *middleman_info;
    gboolean error = FALSE;

    middleman_info = user_data;

    /* Read from the channel, although we don't care about the data,
     * only the resultant status */
    if (condition & G_IO_IN) {
        gsize bytes_read;
        GIOStatus status;
        gchar buf[BUFSIZE];

        status = g_io_channel_read_chars (channel, buf, BUFSIZE, &bytes_read,
                                          NULL);

        switch (status) {
        case G_IO_STATUS_AGAIN:
        case G_IO_STATUS_NORMAL:
            /* Nothing needs to be done (although it would certainly
             * be odd if we just successfully read from the control
             * connection) */
            return TRUE;
        case G_IO_STATUS_EOF:
        case G_IO_STATUS_ERROR:
            /* In either of these cases, we need to shut down the
             * control connection from our end and wait for another
             * one */
            error = TRUE;
            break;
        }
    }

    /* If we've lost our control connection, we need to shut it down
     * from our end and wait for a new one */
    if (error || (condition & (G_IO_ERR | G_IO_HUP))) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Lost control connection, cleaning up");

        gnet_tcp_socket_unref (middleman_info->control);

        middleman_info->control = NULL;
        /* Don't need to remove it manually, because we're going to
         * return FALSE here */
        middleman_info->control_watch = 0;

        rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                          "Waiting for control connection");

        /* Remove this watch */
        return FALSE;
    }

    /* Keep this watch */
    return TRUE;
}

/* Called when something connects to us on the control port */
static void
control_accept (GTcpSocket *server, GTcpSocket *client, gpointer user_data)
{
    MiddlemanInfo *middleman_info;
    GInetAddr *inet_addr;
    char *name;

    middleman_info = user_data;

    if (client == NULL) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Failed to accept control connection");
        return;
    }

    inet_addr = gnet_tcp_socket_get_inetaddr (client);
    name = gnet_inetaddr_get_canonical_name (inet_addr);

    /* We can't have more than one control connection at a time */
    if (middleman_info->control) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Rejecting control connection from %s", name);
        gnet_tcp_socket_unref (client);
        gnet_inetaddr_unref (inet_addr);
        g_free (name);
        return;
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_INFO, rcd_module,
                      "Accepted control connection from %s", name);

    gnet_inetaddr_unref (inet_addr);
    g_free (name);

    middleman_info->control = client;

    /* We don't actually care about any data coming our way (there
     * shouldn't be any, really), we just need to watch for this
     * connection being closed */
    g_io_add_watch (mp_tcp_socket_get_iochannel (client),
                    G_IO_IN | G_IO_ERR | G_IO_HUP,
                    control_func, middleman_info);
}

/* Called when anything connects to us on our client port.  We need to
 * grab the IP address, send it down the control channel along with
 * the sequence number, and then reply to the original connection with
 * the same two byte sequence number.  If anything goes wrong during
 * this process, we just close the client connection. */
static void
client_accept (GTcpSocket *server, GTcpSocket *client, gpointer user_data)
{
    MiddlemanInfo *middleman_info;
    GInetAddr *inet_addr;
    GIOStatus status;
    gchar *name;
    gchar message[6];
    guint16 sequence;
    struct in_addr addr;

    middleman_info = user_data;

    if (client == NULL) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Unrecoverable error accepting client connection");
        return;
    }

    inet_addr = gnet_tcp_socket_get_inetaddr (client);
    name = gnet_inetaddr_get_canonical_name (inet_addr);

    /* Make sure we have a control connection */
    if (middleman_info->control == NULL) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Rejecting client connection from %s", name);
        gnet_tcp_socket_unref (client);
        gnet_inetaddr_unref (inet_addr);
        g_free (name);
        return;
    }

    sequence = g_htons (middleman_info->sequence);
    middleman_info->sequence++;
    if (!middleman_info->sequence)
        middleman_info->sequence++;
    memcpy (message, &sequence, 2);
    sequence = g_ntohs (sequence);

    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                      "Received connection request from %s", name);

    /*
     * Workaround for when we get a connection over the loopback device.
     * We obviously can't pass along 127.0.0.1 to the server, so we try
     * to get our internet address and use it instead.  If we can't get
     * one, it's presumably because we don't have another interface and
     * our server has to be running on our own machine for things to work
     * anyway, so we let it through.
     */
    if (gnet_inetaddr_is_loopback (inet_addr)) {
        GInetAddr *net_addr;

        net_addr = gnet_inetaddr_autodetect_internet_interface ();

        if (net_addr) {
            gnet_inetaddr_unref (inet_addr);
            g_free (name);

            inet_addr = net_addr;
            name = gnet_inetaddr_get_canonical_name (inet_addr);

            rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                              "Loopback connection request; trying %s", name);
        }
    }

    gnet_inetaddr_unref (inet_addr);

    if (!inet_aton (name, &addr)) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "%s is not a valid IPv4 address", name);
        g_free (name);
        gnet_tcp_socket_unref (client);
        return;
    }
    g_free (name);
    memcpy (message + 2, &addr.s_addr, 4);

    status = mp_io_channel_write_chars (
        mp_tcp_socket_get_iochannel (middleman_info->control), message, 6);

    switch (status) {
    case G_IO_STATUS_AGAIN:
        /* This really shouldn't happen */
        g_assert_not_reached ();
    case G_IO_STATUS_EOF:
    case G_IO_STATUS_ERROR:
        /* If we're unable to communicate successfully with our
         * control connection, we'd better shut this one down and wait
         * for a new one.  And, of course, close down the client
         * connection too */
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Error writing to control connection, closing");

        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Rejecting client connection");
        gnet_tcp_socket_unref (client);

        gnet_tcp_socket_unref (middleman_info->control);
        middleman_info->control = NULL;
        g_source_remove (middleman_info->control_watch);
        middleman_info->control_watch = 0;

        rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                          "Waiting for control connection");
        return;
    case G_IO_STATUS_NORMAL:
        /* Success */
        break;
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                      "Connection request forwarded to server as "
                      "connection id %d", sequence);

    status = mp_io_channel_write_chars (
        mp_tcp_socket_get_iochannel (client), message, 2);

    /* We gave it a good shot, but if we didn't succeed at writing to
     * the client, there's not much we can do... */
    rcd_module_debug (RCD_DEBUG_LEVEL_INFO, rcd_module,
                      "Closing client connection");
    gnet_tcp_socket_unref (client);
}

static void
shutdown_module (gpointer user_data)
{
    MiddlemanInfo *middleman_info = user_data;

    rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Shutting down module");

    if (middleman_info->control_server)
        gnet_tcp_socket_delete (middleman_info->control_server);

    if (middleman_info->client_server)
        gnet_tcp_socket_delete (middleman_info->client_server);

    if (middleman_info->control)
        gnet_tcp_socket_delete (middleman_info->control);
}

void
rcd_module_load (RCDModule *module)
{
    const char *bind_ip;

    /* Initialize the module */
    module->name = "rcd.magic_proxy.middleman";
    module->description = "The Magic Proxy Middleman module";
    module->version = "0.0";
    module->interface_major = 1;
    module->interface_minor = 0;

    rcd_module = module;

    bind_ip = rcd_prefs_get_string ("/Server/bind-ip");

    memset (&middleman_info, 0, sizeof (middleman_info));

    middleman_info.sequence = 1;

    if ((middleman_info.control_server =
         mp_create_server (CONTROL_PORT, bind_ip, 3, 1, control_accept,
                           &middleman_info)) == NULL)
    {
        rcd_module_debug (RCD_DEBUG_LEVEL_CRITICAL, rcd_module,
                          "Unable to create control server");
        exit (-1);
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Waiting for control connection");

    if ((middleman_info.client_server =
         mp_create_server (CLIENT_PORT, bind_ip, 3, 1, client_accept,
                           &middleman_info)) == NULL)
    {
        rcd_module_debug (RCD_DEBUG_LEVEL_CRITICAL, rcd_module,
                          "Unable to create client server");
        exit (-1);
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Waiting for client connections");

    rcd_shutdown_add_handler (shutdown_module, &middleman_info);
}

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;
