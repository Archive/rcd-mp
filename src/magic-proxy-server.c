/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <glib.h>
#include <glib-object.h>
#include <gnet.h>

#include "magic-proxy-common.h"

#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define CONTROL_PORT 2828
#define CLIENT_PORT  2830

#define PACKET_LENGTH 6

#define MIDDLEMAN_LIST SYSCONFDIR"/rcserver/magic-proxy"
#define LOCKFILE "/tmp/.magic-proxy-server-lock"

static const char *process_name;
static gboolean daemonic = FALSE;

static const gchar *target_host;
static guint16 target_port;

static GHashTable *middleman_hash = NULL;

typedef struct {
    char *host;

    GSList *clients;
    GSList *pending;
    GSList *connections;
} ProxyInfo;

typedef struct {
    GTcpSocket *control;
    guint       watch;
    gchar       buf[PACKET_LENGTH];
    gsize       buf_size;
    ProxyInfo  *proxy_info;
} ControlState;

typedef struct {
    guint16                   sequence;
    GTcpSocket               *socket;
    guint                     timeout;
    GTcpSocketConnectAsyncID  client_connect_id;
    GTcpSocketConnectAsyncID  target_connect_id;
    char                     *address;
    ProxyInfo                *proxy_info;
} PendingConnection;

static void
log (const char *format, ...)
{
    va_list args;
    char *str;

    va_start (args, format);
    str = g_strdup_vprintf (format, args);
    va_end (args);

    if (daemonic) {
        char *log_name = g_strdup_printf ("%s[%d]", process_name, getpid());

        openlog (log_name, 0, LOG_DAEMON);
        syslog (LOG_INFO, "%s", str);
        closelog ();
        g_free (log_name);
    }
    else
        g_print ("%s\n", str);
    
    g_free (str);
}

static ControlState *
control_state_new (ProxyInfo *proxy_info)
{
    ControlState *control_state;

    control_state = g_new0 (ControlState, 1);

    control_state->proxy_info = proxy_info;

    proxy_info->clients = g_slist_append (
        proxy_info->clients, control_state);

    return control_state;
}

static void
control_state_delete (ControlState *control_state)
{
    if (control_state->watch)
        g_source_remove (control_state->watch);

    if (control_state->control)
        gnet_tcp_socket_unref (control_state->control);

    if (control_state->proxy_info)
        control_state->proxy_info->clients =
            g_slist_remove (control_state->proxy_info->clients, control_state);

    g_free (control_state);
}

static PendingConnection *
pending_connection_new (ProxyInfo *proxy_info)
{
    PendingConnection *pending_connection;

    pending_connection = g_new0 (PendingConnection, 1);

    pending_connection->proxy_info = proxy_info;

    proxy_info->pending = g_slist_append (
        proxy_info->pending, pending_connection);

    return pending_connection;
}

static void
pending_connection_delete (PendingConnection *pending_connection)
{
    if (pending_connection->socket)
        gnet_tcp_socket_unref (pending_connection->socket);

    if (pending_connection->timeout)
        g_source_remove (pending_connection->timeout);

    if (pending_connection->client_connect_id)
        gnet_tcp_socket_connect_async_cancel (
            pending_connection->client_connect_id);

    if (pending_connection->target_connect_id)
        gnet_tcp_socket_connect_async_cancel (
            pending_connection->target_connect_id);

    if (pending_connection->proxy_info)
        pending_connection->proxy_info->pending = g_slist_remove (
            pending_connection->proxy_info->pending, pending_connection);

    if (pending_connection->address)
        g_free (pending_connection->address);

    g_free (pending_connection);
}

static void
target_connect (GTcpSocket *socket, GInetAddr *addr,
                GTcpSocketConnectAsyncStatus status, gpointer user_data)
{
    PendingConnection *pending_connection;
    ProxyInfo *proxy_info;

    if (addr)
        gnet_inetaddr_unref (addr);

    pending_connection = user_data;
    proxy_info = pending_connection->proxy_info;

    pending_connection->target_connect_id = NULL;

    if (status != GTCP_SOCKET_CONNECT_ASYNC_STATUS_OK) {
        log ("Unable to connect to target: %s:%d", target_host, target_port);
        pending_connection_delete (pending_connection);

        return;
    }

    mp_socket_tunnel_create (pending_connection->socket, socket,
                             &proxy_info->connections);
    gnet_tcp_socket_unref (socket);

    pending_connection_delete (pending_connection);
}

static gboolean
pending_connection_timeout (gpointer user_data)
{
    PendingConnection *pending_connection;

    pending_connection = user_data;

    pending_connection_delete (pending_connection);

    return FALSE;
}

static void
client_connect (GTcpSocket *socket, GInetAddr *addr,
                GTcpSocketConnectAsyncStatus status, gpointer user_data)
{
    PendingConnection *pending_connection;
    guint16 sequence;
    gchar message[2];

    if (addr)
        gnet_inetaddr_unref (addr);

    pending_connection = user_data;

    pending_connection->client_connect_id = NULL;

    if (status != GTCP_SOCKET_CONNECT_ASYNC_STATUS_OK) {
        log ("Unable to connect to client %s", pending_connection->address);

        pending_connection_delete (pending_connection);

        return;
    }

    sequence = g_htons (pending_connection->sequence);
    memcpy (message, &sequence, 2);

    switch (mp_io_channel_write_chars (
                mp_tcp_socket_get_iochannel (socket), message, 2))
    {
    case G_IO_STATUS_AGAIN:
        g_assert_not_reached ();
    case G_IO_STATUS_ERROR:
    case G_IO_STATUS_EOF:
        gnet_tcp_socket_unref (socket);

        pending_connection_delete (pending_connection);

        return;
    case G_IO_STATUS_NORMAL:
        /* Do nothing */
        break;
    }

    pending_connection->socket = socket;

    pending_connection->timeout = g_timeout_add (
        TIMEOUT, pending_connection_timeout,
        pending_connection);

    pending_connection->target_connect_id =
        gnet_tcp_socket_connect_async (target_host, target_port,
                                       target_connect, pending_connection);
}

static gboolean
control_func (GIOChannel *channel, GIOCondition condition, gpointer user_data)
{
    ControlState *control_state;
    gboolean error = FALSE;

    control_state = user_data;

    if (condition & G_IO_IN) {
        GIOStatus status;
        gsize bytes_read;

        status = g_io_channel_read_chars (
            channel, control_state->buf + control_state->buf_size,
            PACKET_LENGTH - control_state->buf_size, &bytes_read, NULL);

        MP_DEBUG ("Read %d bytes from fd %d\n", bytes_read,
                  g_io_channel_unix_get_fd (channel));

        switch (status) {
        case G_IO_STATUS_AGAIN:
            /* We'll try this again later */
            return TRUE;
        case G_IO_STATUS_EOF:
        case G_IO_STATUS_ERROR:
            /* Control connection broken */
            error = TRUE;
        case G_IO_STATUS_NORMAL:
            control_state->buf_size += bytes_read;
            if (control_state->buf_size == PACKET_LENGTH) {
                PendingConnection *pending_connection;

                pending_connection = pending_connection_new (
                    control_state->proxy_info);

                memcpy (&pending_connection->sequence,
                        control_state->buf, 2);
                pending_connection->sequence =
                    g_ntohs (pending_connection->sequence);

                pending_connection->proxy_info =
                    control_state->proxy_info;

                pending_connection->address = g_strdup_printf (
                    "%hhu.%hhu.%hhu.%hhu",
                    control_state->buf[2],
                    control_state->buf[3],
                    control_state->buf[4],
                    control_state->buf[5]);

                control_state->buf_size = 0;

                pending_connection->client_connect_id =
                    gnet_tcp_socket_connect_async (
                        pending_connection->address, CLIENT_PORT,
                        client_connect, pending_connection);
            }
        }
    }

    if (error || (condition & (G_IO_HUP | G_IO_ERR))) {
        log ("Control connection to %s lost", control_state->proxy_info->host);
        control_state_delete (control_state);
    }

    return TRUE;
}

/* Called when we establish a control connection */
static void
control_connect (GTcpSocket *socket, GInetAddr *addr,
                 GTcpSocketConnectAsyncStatus status, gpointer user_data)
{
    ProxyInfo *proxy_info;
    ControlState *control_state;

    if (addr)
        gnet_inetaddr_unref (addr);

    proxy_info = user_data;

    if (status != GTCP_SOCKET_CONNECT_ASYNC_STATUS_OK) {
        log ("Unable to connect to middleman at %s", proxy_info->host);
        return;
    }

    log ("Control connection established with %s", proxy_info->host);

    control_state = control_state_new (proxy_info);

    control_state->control = socket;

    control_state->watch =
        g_io_add_watch (
            mp_tcp_socket_get_iochannel (socket),
            G_IO_IN | G_IO_ERR | G_IO_HUP,
            control_func, control_state);
}

static void
create_control (const gchar *host, ProxyInfo *proxy_info)
{
    log ("Creating control connection to %s", host);
    gnet_tcp_socket_connect_async (host, CONTROL_PORT, control_connect,
                                   proxy_info);
}

static void
proxy_info_free (ProxyInfo *proxy_info)
{
    g_slist_free (proxy_info->clients);
    g_slist_free (proxy_info->pending);
    g_slist_free (proxy_info->connections);

    g_free (proxy_info);
}

static void
remove_middleman_from_hash (const char *host)
{
    ProxyInfo *proxy_info = g_hash_table_lookup (middleman_hash, host);
    GSList *iter, *next;

    for (iter = proxy_info->pending; iter; iter = next) {
        next = iter->next;

        pending_connection_delete (iter->data);
    }

    for (iter = proxy_info->clients; iter; iter = next) {
        next = iter->next;

        control_state_delete (iter->data);
    }

    /* 
     * FIXME: Should probably shut down ->connections, but it's just a
     * pointless list of opaque structures right now.
     */

    g_hash_table_remove (middleman_hash, host);
}

static void
add_middleman_to_hash (const char *host)
{
    ProxyInfo *proxy_info;

    proxy_info = g_new0 (ProxyInfo, 1);
    proxy_info->host = g_strdup (host);

    g_hash_table_insert (middleman_hash, proxy_info->host, proxy_info);
    create_control (host, proxy_info);
}

typedef struct {
    char **middlemen;
    GSList *middlemen_to_remove;
} MiddlemanInfo;

static void
remove_old_middlemen (gpointer key, gpointer value, gpointer user_data)
{
    char *host = key;
    MiddlemanInfo *mi = user_data;
    char **iter;

    for (iter = mi->middlemen; *iter; iter++) {
        if (g_strcasecmp (host, *iter) == 0) {
            return;
        }
    }

    mi->middlemen_to_remove = g_slist_prepend (mi->middlemen_to_remove, host);
}

static gboolean
read_middleman_list (void)
{
    char *contents;
    int len;
    MiddlemanInfo mi;
    int i;
    GSList *iter;

    mi.middlemen = NULL;
    mi.middlemen_to_remove = NULL;

    if (g_file_get_contents (MIDDLEMAN_LIST, &contents, &len, NULL)) {
        mi.middlemen = g_strsplit (contents, "\n", 0);
        g_free (contents);

        for (i = 0; mi.middlemen[i]; i++) {
            const char *m = g_strstrip (mi.middlemen[i]);

            if (*m && *m != '#' && !g_hash_table_lookup (middleman_hash, m))
                add_middleman_to_hash (m);
        }
    }

    /* Remove connections to old middlemen which aren't around anymore */
    g_hash_table_foreach (middleman_hash, remove_old_middlemen, &mi);
    for (iter = mi.middlemen_to_remove; iter; iter = iter->next)
        remove_middleman_from_hash (iter->data);

    g_strfreev (mi.middlemen);
    g_slist_free (mi.middlemen_to_remove);

    return TRUE;
}

static void
resurrect_conn (gpointer key,
                gpointer value,
                gpointer user_data)
{
    const char *host = key;
    ProxyInfo *proxy_info = value;

    if (!proxy_info->clients)
        create_control (host, proxy_info);
}

static gboolean
resurrect_middleman_connections (gpointer user_data)
{
    g_hash_table_foreach (middleman_hash, resurrect_conn, NULL);

    return TRUE;
}

static void
daemonize (void)
{
    int fork_rv;
    int i;
    int fd;

    fork_rv = fork ();
    if (fork_rv < 0) {
        g_printerr ("%s: fork failed!\n", process_name);
        unlink (LOCKFILE);
        exit (-1);
    }

    /* Parent exits */
    if (fork_rv > 0)
        exit (0);

    /* Set our own process group */
    setsid ();

    /* Close all file descriptors */
    for (i = getdtablesize (); i >= 0; --i)
        close (i);

    /* open /dev/null as stdin, stdout, stderr */
    fd = open ("/dev/null", O_RDWR);
    g_assert (fd == STDIN_FILENO);

    fd = open ("/dev/null", O_RDWR);
    g_assert (fd == STDOUT_FILENO);

    fd = open ("/dev/null", O_RDWR);
    g_assert (fd == STDERR_FILENO);

    daemonic = TRUE;

    log ("Starting magic-proxy-server");
}

static void
signal_handler (int sig_num)
{
    const char *sig_name = NULL;

    if (sig_num == SIGQUIT)
        sig_name = "SIGQUIT";
    else if (sig_num == SIGTERM)
        sig_name = "SIGTERM";
    else if (sig_num == SIGINT)
        sig_name = "SIGINT";
    else
        g_assert_not_reached ();

    log ("Received %s... Shutting down.", sig_name);
    
    unlink (LOCKFILE);

    exit (0);
}

static void
setup_signal_handlers (void)
{
    struct sigaction sig_action;

    sig_action.sa_handler = signal_handler;
    sigemptyset (&sig_action.sa_mask);
    sig_action.sa_flags = 0;
    sigaction (SIGINT,  &sig_action, NULL);
    sigaction (SIGTERM, &sig_action, NULL);
    sigaction (SIGQUIT, &sig_action, NULL);
}

int
main (int argc, char **argv)
{
    GMainLoop *main_loop;
    int argv_offset = 0;
    int fd;

    process_name = g_basename (argv[0]);

    g_type_init ();

    if (argc > 1 && g_strcasecmp (argv[1], "-n") == 0)
        argv_offset++;

    if (argc == argv_offset + 1) {
        target_host = "localhost";
        target_port = 443;
    }
    else if (argc == argv_offset + 3) {
        target_host = argv[argv_offset + 1];
        target_port = atoi (argv[argv_offset + 2]);
    }
    else {
        g_printerr ("Usage: %s [-n] <target host> <target port>\n",
                    process_name);
        exit (-1);
    }

    fd = open (LOCKFILE, O_WRONLY | O_CREAT | O_EXCL, 0644);
    if (fd < 0) {
        g_printerr ("Unable to create lockfile " LOCKFILE "\n");
        g_printerr ("Is magic-proxy-server already running?\n");
        exit (-1);
    }

    if (!argv_offset)
        daemonize ();

    setup_signal_handlers ();

    middleman_hash = g_hash_table_new_full (g_str_hash, g_str_equal,
                                            NULL,
                                            (GDestroyNotify) proxy_info_free);

    read_middleman_list ();

    g_timeout_add (5000, (GSourceFunc) read_middleman_list, NULL);
    g_timeout_add (5000, resurrect_middleman_connections, NULL);

    main_loop = g_main_loop_new (NULL, TRUE);
    g_main_loop_run (main_loop);
    g_main_loop_unref (main_loop);

    unlink (LOCKFILE);

    return 0;
}
