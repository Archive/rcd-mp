/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <glib.h>
#include <glib-object.h>
#include <gnet.h>

#include "magic-proxy-common.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <rcd-module.h>
#include <rcd-prefs.h>
#include <rcd-rpc-prefs.h>
#include <rcd-shutdown.h>

#define SERVER_PORT 2830
#define CLIENT_PORT 2831
#define MIDDLEMAN_PORT 2829

#define PACKET_LENGTH 2

typedef struct {
    GTcpSocket *client_server;
    GTcpSocket *server_server;
    GSList     *pending_clients; /* List of PendingClient */
    GSList     *pending_servers; /* List of PendingServer */
    GSList     *connections;     /* List of MPSocketTunnel */
} ClientInfo;

typedef struct {
    guint16                   sequence;
    GTcpSocket               *socket;
    GTcpSocket               *middleman_socket;
    GTcpSocketConnectAsyncID  middleman_connect_id;
    guint                     middleman_watch;
    guint                     timeout;
    gchar                     buf[PACKET_LENGTH];
    gsize                     bytes_read;
    ClientInfo               *client_info;
    char                    **middleman_list;
    int                       middleman_index;
} PendingClient;

typedef struct {
    guint16     sequence;
    GTcpSocket *socket;
    guint       watch;
    guint       timeout;
    gchar       buf[PACKET_LENGTH];
    gsize       bytes_read;
    ClientInfo *client_info;
} PendingServer;

static RCDModule *rcd_module;
static ClientInfo client_info;

static void middleman_connect (GTcpSocket *socket, GInetAddr *addr,
                               GTcpSocketConnectAsyncStatus status,
                               gpointer user_data);

static const char *
get_middleman_host (void)
{
    return rcd_prefs_get_string ("/MagicProxy/middleman-host=localhost");
}

static void
set_middleman_host (const char *str)
{
    rcd_prefs_set_string ("/MagicProxy/middleman-host", str);
}

static PendingClient *
pending_client_new (ClientInfo *client_info)
{
    PendingClient *pending_client;

    pending_client = g_new0 (PendingClient, 1);

    pending_client->client_info = client_info;

    client_info->pending_clients = g_slist_append (
        client_info->pending_clients, pending_client);

    return pending_client;
}

static void
pending_client_delete (PendingClient *pending_client)
{
    if (pending_client->socket)
        gnet_tcp_socket_unref (pending_client->socket);

    if (pending_client->timeout)
        g_source_remove (pending_client->timeout);

    if (pending_client->client_info)
        pending_client->client_info->pending_clients = g_slist_remove (
            pending_client->client_info->pending_clients, pending_client);

    if (pending_client->middleman_list)
        g_strfreev (pending_client->middleman_list);

    g_free (pending_client);
}

static void
pending_client_next (PendingClient *pending_client)
{
    if (pending_client->middleman_socket)
        gnet_tcp_socket_unref (pending_client->middleman_socket);

    if (pending_client->middleman_connect_id)
        gnet_tcp_socket_connect_async_cancel (
            pending_client->middleman_connect_id);

    if (pending_client->middleman_watch)
        g_source_remove (pending_client->middleman_watch);

    if (pending_client->middleman_list[pending_client->middleman_index] &&
        pending_client->middleman_list[++pending_client->middleman_index])
    {
        pending_client->middleman_connect_id =
            gnet_tcp_socket_connect_async (
                pending_client->middleman_list[pending_client->middleman_index],
                MIDDLEMAN_PORT, middleman_connect, pending_client);
    }
    else {
        pending_client_delete (pending_client);
    }
}

static PendingServer *
pending_server_new (ClientInfo *client_info)
{
    PendingServer *pending_server;

    pending_server = g_new0 (PendingServer, 1);

    pending_server->client_info = client_info;

    client_info->pending_servers = g_slist_append (
        client_info->pending_servers, pending_server);

    return pending_server;
}

static void
pending_server_delete (PendingServer *pending_server)
{
    if (pending_server->socket)
        gnet_tcp_socket_unref (pending_server->socket);

    if (pending_server->watch)
        g_source_remove (pending_server->watch);

    if (pending_server->timeout)
        g_source_remove (pending_server->timeout);

    if (pending_server->client_info)
        pending_server->client_info->pending_servers = g_slist_remove (
            pending_server->client_info->pending_servers, pending_server);

    g_free (pending_server);
}

/* Given a PendingClient and a sequence number, returns 0 if the
 * PendingClient has that sequence number, and non-zero otherwise */
static gint
pending_client_find_sequence (gconstpointer a, gconstpointer b)
{
    const PendingClient *pending_client;
    guint16 sequence;

    pending_client = a;
    sequence = GPOINTER_TO_INT (b);

    g_assert (pending_client);

    if (pending_client->sequence == sequence)
        return 0;

    return 1;
}

/* Given a PendingServer and a sequence number, returns TRUE if the
 * PendingServer has that sequence number, and non-zero otherwise */
static gint
pending_server_find_sequence (gconstpointer a, gconstpointer b)
{
    const PendingServer *pending_server;
    guint16 sequence;

    pending_server = a;
    sequence = GPOINTER_TO_INT (b);

    if (pending_server->sequence == sequence)
        return 0;

    return 1;
}

/* Given a PendingClient and PendingServer, create two MPSocketTunnel
 * structs from them, tied together, add them to the ClientInfo
 * connection list, add the required watches, and destroy the
 * PendingClient and PendingServer, removing them from the
 * client_info */
static void
make_pair (PendingClient *pending_client,
           PendingServer *pending_server)
{
    ClientInfo *client_info;

    g_assert (pending_client);
    g_assert (pending_server);

    g_assert (pending_client->client_info == pending_server->client_info);
    g_assert (pending_client->sequence == pending_server->sequence);

    client_info = pending_client->client_info;

    /* Create the socket tunnel */
    mp_socket_tunnel_create (pending_client->socket, pending_server->socket,
                             &client_info->connections);

    /* Clean up the pending client/server */
    pending_client_delete (pending_client);
    pending_server_delete (pending_server);
}

/* Watch function on the server connection (at least, until it's been
 * tied to an MPSocketTunnel).  We're expecting to get the sequence
 * number */
static gboolean
server_func (GIOChannel *channel, GIOCondition condition, gpointer user_data)
{
    PendingServer *pending_server;
    gboolean error = FALSE;

    pending_server = user_data;

    if (condition & G_IO_IN) {
        GIOStatus status;
        gsize bytes_read;

        status = g_io_channel_read_chars (
            channel, pending_server->buf + pending_server->bytes_read,
            PACKET_LENGTH - pending_server->bytes_read, &bytes_read, NULL);

        rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                          "Read %d bytes from fd %d",
                          bytes_read, g_io_channel_unix_get_fd (channel));

        switch (status) {
        case G_IO_STATUS_AGAIN:
            /* We'll try this again later */
            return TRUE;
        case G_IO_STATUS_EOF:
        case G_IO_STATUS_ERROR:
            /* Badness */
            error = TRUE;
            break;
        case G_IO_STATUS_NORMAL:
            pending_server->bytes_read += bytes_read;

            /* Have we received a full sequence number? */
            if (pending_server->bytes_read == PACKET_LENGTH) {
                GSList *client;

                memcpy (&pending_server->sequence,
                        pending_server->buf, PACKET_LENGTH);
                pending_server->sequence = g_ntohs (
                    pending_server->sequence);

                rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module, 
                                  "Server connection has sequence number %d",
                                  pending_server->sequence);

                /* Check and see if we've got a client waiting with
                 * this sequence number */
                if ((client = g_slist_find_custom (
                         pending_server->client_info->pending_clients,
                         GINT_TO_POINTER ((guint32) pending_server->sequence),
                         pending_client_find_sequence)))
                {
                    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                                      "Establishing connection tunnel for "
                                      "sequence number %d",
                                      pending_server->sequence);
                    make_pair (client->data, pending_server);
                }

                return FALSE;
            }
            break;
        }
    }

    /* Something went wrong, so we'd better just drop this */
    if (error || (condition & (G_IO_ERR | G_IO_HUP))) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Dropping unknown server connection");

        pending_server_delete (pending_server);

        return FALSE;
    }

    return TRUE;
}

static gboolean
pending_server_timeout (gpointer user_data)
{
    PendingServer *pending_server;

    pending_server = user_data;

    rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                      "Server connection %d timed out",
                      pending_server->sequence);

    pending_server_delete (pending_server);

    return FALSE;
}

/* Something has connected to our listening socket for server
 * connections.  Create a PendingServer, put a watch and timeout on
 * it, and wait for it to give us a sequence number */
static void
server_accept (GTcpSocket *server, GTcpSocket *client, gpointer user_data)
{
    ClientInfo *client_info;
    PendingServer *pending_server;

    client_info = user_data;

    if (client == NULL) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Failed to accept server connection");
        return;
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_INFO, rcd_module,
                      "Accepted server connection");

    pending_server = pending_server_new (client_info);

    pending_server->socket = client;

    pending_server->watch = g_io_add_watch (
        mp_tcp_socket_get_iochannel (client),
        G_IO_IN | G_IO_ERR | G_IO_HUP,
        server_func, pending_server);

    pending_server->timeout =
        g_timeout_add (TIMEOUT, pending_server_timeout,
                       pending_server);
}

/* Watch function on the middleman.  We're expecting a sequence number
 * back from this connection, so that we can match this client with a
 * server connection */
static gboolean
middleman_func (GIOChannel *channel, GIOCondition condition,
                gpointer user_data)
{
    PendingClient *pending_client;
    gboolean error = FALSE;

    pending_client = user_data;

    if (condition & G_IO_IN) {
        GIOStatus status;
        gsize bytes_read;

        status = g_io_channel_read_chars (
            channel, pending_client->buf + pending_client->bytes_read,
            PACKET_LENGTH - pending_client->bytes_read, &bytes_read, NULL);

        rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                          "Read %d bytes from fd %d",
                          bytes_read, g_io_channel_unix_get_fd (channel));

        switch (status) {
        case G_IO_STATUS_AGAIN:
            /* We'll try this again later */
            return TRUE;
        case G_IO_STATUS_EOF:
        case G_IO_STATUS_ERROR:
            /* Badness */
            error = TRUE;
            break;
        case G_IO_STATUS_NORMAL:
            pending_client->bytes_read += bytes_read;

            /* Have we got the entire sequence number yet? */
            if (pending_client->bytes_read == PACKET_LENGTH) {
                GSList *server;

                memcpy (&pending_client->sequence,
                        pending_client->buf, PACKET_LENGTH);
                pending_client->sequence = g_ntohs (
                    pending_client->sequence);
                gnet_tcp_socket_unref (pending_client->middleman_socket);
                pending_client->middleman_socket = NULL;
                g_source_remove (pending_client->middleman_watch);
                pending_client->middleman_watch = 0;

                rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module, 
                                  "Client connection has sequence number %d",
                                  pending_client->sequence);

                /* If the server is already waiting for us (possible,
                 * I think?), create the tunnel immediately */
                if ((server = g_slist_find_custom (
                        pending_client->client_info->pending_servers,
                        GINT_TO_POINTER ((guint32) pending_client->sequence),
                        pending_server_find_sequence)))
                {
                    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module, 
                                      "Establishing connection tunnel %d",
                                      pending_client->sequence);
                    make_pair (pending_client, server->data);
                }

                return FALSE;
            }
            break;
        }
    }

    /* Something went wrong, so give up */
    if (error || (condition & (G_IO_ERR | G_IO_HUP))) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module, 
                          "Dropping unknown middleman connection");

        pending_client_next (pending_client);

        return FALSE;
    }

    return TRUE;
}

static gboolean
pending_client_timeout (gpointer user_data)
{
    PendingClient *pending_client;

    pending_client = user_data;

    rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module, 
                      "Client connection %d timed out\n",
                      pending_client->sequence);

    pending_client_next (pending_client);

    return FALSE;
}

/* Called once we've connected to the middleman */
static void
middleman_connect (GTcpSocket *socket, GInetAddr *addr,
                   GTcpSocketConnectAsyncStatus status, gpointer user_data)
{
    PendingClient *pending_client;

    if (addr)
        gnet_inetaddr_unref (addr);

    pending_client = user_data;

    pending_client->middleman_connect_id = NULL;

    /* The connection failed, so drop this pending connection */
    if (status != GTCP_SOCKET_CONNECT_ASYNC_STATUS_OK) {
        rcd_module_debug (
            RCD_DEBUG_LEVEL_WARNING, rcd_module, 
            "Unable to connect to middleman at %s",
            pending_client->middleman_list[pending_client->middleman_index]);

        pending_client_next (pending_client);

        return;
    }

    rcd_module_debug (
        RCD_DEBUG_LEVEL_DEBUG, rcd_module,
        "Connected to proxy middleman at %s",
        pending_client->middleman_list[pending_client->middleman_index]);

    pending_client->middleman_socket = socket;

    /* Wait for a sequence number */
    pending_client->middleman_watch =
        g_io_add_watch (
            mp_tcp_socket_get_iochannel (socket),
            G_IO_IN | G_IO_ERR | G_IO_HUP,
            middleman_func, pending_client);

    /* Restart the timeout */
    g_source_remove (pending_client->timeout);
    pending_client->timeout =
        g_timeout_add (TIMEOUT, pending_client_timeout,
                       pending_client);
}

static char **
parse_middleman_list (const char *list)
{
    char **mm;
    int i;

    mm = g_strsplit (list, ",", 0);

    for (i = 0; mm && mm[i]; i++)
        g_strstrip (mm[i]);

    return mm;
}

/* Called when a client (ie an rcd) has connected to us.  Need to
 * store this connection (with a timeout), and signal the middleman
 * that we need a connection from the server */
static void
client_accept (GTcpSocket *server, GTcpSocket *client, gpointer user_data)
{
    ClientInfo *client_info;
    PendingClient *pending_client;

    client_info = user_data;

    if (client == NULL) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Failed to accept client connection");
        return;
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_INFO, rcd_module,
                      "Accepted client connection");

    pending_client = pending_client_new (client_info);

    pending_client->socket = client;

    pending_client->timeout =
        g_timeout_add (TIMEOUT, pending_client_timeout,
                       pending_client);

    pending_client->middleman_list =
        parse_middleman_list (get_middleman_host ());

    /* Signal the middleman that we need a connection */
    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module, 
                      "Signaling proxy middleman for connection from server");

    pending_client->middleman_connect_id =
        gnet_tcp_socket_connect_async (pending_client->middleman_list[0],
                                       MIDDLEMAN_PORT, middleman_connect,
                                       pending_client);
}

static void
shutdown_module (gpointer user_data)
{
    ClientInfo *client_info = user_data;
    GSList *iter, *next;

    rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Shutting down module");

    if (client_info->client_server)
        gnet_tcp_socket_delete (client_info->client_server);

    if (client_info->server_server)
        gnet_tcp_socket_delete (client_info->server_server);

    for (iter = client_info->pending_clients; iter; iter = next) {
        PendingClient *pending_client = iter->data;

        next = iter->next;

        pending_client_delete (pending_client);
    }

    for (iter = client_info->pending_servers; iter; iter = next) {
        PendingServer *pending_server = iter->data;

        next = iter->next;

        pending_server_delete (pending_server);
    }
}

void
rcd_module_load (RCDModule *module)
{
    const char *bind_ip;

    /* Initialize the module */
    module->name = "rcd.magic_proxy.client";
    module->description = "The Magic Proxy Client module";
    module->version = "0.0";
    module->interface_major = 1;
    module->interface_minor = 0;

    rcd_module = module;

    rcd_rpc_prefs_register_pref (
        "middleman-host", RCD_PREF_STRING,
        "Host of the magic-proxy dispatcher",
        "Advanced",
        (RCDPrefGetFunc) get_middleman_host, "view",
        (RCDPrefSetFunc) set_middleman_host, "superuser");

    bind_ip = rcd_prefs_get_string ("/Server/bind-ip");

    memset (&client_info, 0, sizeof (client_info));

    if ((client_info.client_server =
         mp_create_server (CLIENT_PORT, bind_ip, 3, 1, client_accept,
                           &client_info)) == NULL)
    {
        rcd_module_debug (RCD_DEBUG_LEVEL_CRITICAL, rcd_module,
                          "Unable to create client listening socket");
        exit (-1);
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Waiting for client connections");

    if ((client_info.server_server =
         mp_create_server (SERVER_PORT, bind_ip, 3, 1, server_accept,
                           &client_info)) == NULL)
    {
        rcd_module_debug (RCD_DEBUG_LEVEL_CRITICAL, rcd_module,
                          "Unable to create server listening socket");
        exit (-1);
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Waiting for server connections");

    rcd_shutdown_add_handler (shutdown_module, &client_info);
}

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;
