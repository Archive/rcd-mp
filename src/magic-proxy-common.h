#ifndef _MAGIC_PROXY_COMMON_H
#define _MAGIC_PROXY_COMMON_H

#include <stdio.h>

#define BUFSIZE 16384

#define TIMEOUT 30000

#ifdef ENABLE_DEBUG
#define MP_DEBUG(format...) fprintf(stderr, format);
#else
#define MP_DEBUG(format...) ;
#endif

GIOChannel *mp_tcp_socket_get_iochannel (GTcpSocket *socket);

void mp_socket_tunnel_create (GTcpSocket *socket1, GTcpSocket *socket2,
                              GSList **connections);

GTcpSocket *mp_create_server (guint16 port, const gchar *name, guint retries,
                              guint delay, GTcpSocketAcceptFunc func,
                              gpointer user_data);

GIOStatus mp_io_channel_write_chars (GIOChannel *channel, const gchar *buf,
                                     gsize count);

#endif
